<script>
window.onload=function(){
var css=document.createElement("link");
css.setAttribute("rel","stylesheet");
css.setAttribute("type","text/css");
css.setAttribute("href","/common/css/header.css");
document.getElementsByTagName("head")[0].appendChild(css);
// document.getElementById('wrapperTop').style.display = "block";

css.onload = function(){
$("#wrapperTop").slideDown("slow");
}
}
</script>

<div style="display:none;" id="wrapperTop">
 <header id="headerArea" class="inner">
 <h1 id="siteID"><a href="/"><img src="http://techkidscamp.jp/common/img/hdr_img_siteID.png" alt="Tech Kids CAMPとは Let&#39;s say Hello! to the world."></a></h1>
 <p id="menu" class="icon-menu"></p>
   <nav id="nav">
     <ul id="globalNav">
       <li><a href="/camp">キャンプ</a></li>
       <li><a href="/school">スクール</a></li>
       <li><a href="/hometeacher">ホームティーチャー</a></li>
       <li><a href="/blog">ブログ</a></li>
       <li><a href="https://reg31.smp.ne.jp/regist/is?SMPFORM=lgke-obqir-fbf39d30ea3e194bf47fe470a73a672e">メルマガ</a></li>
     </ul>
     <p id="contact"><img src="http://techkidscamp.jp/common/img/hdr_img_contact.png" alt="お問い合わせは03-5459-0212 株式会社CA Tech Kids"></p>
     <div class="nav_outer">
       <ul id="socialNav">
         <li><a href="https://www.facebook.com/techkids.jp" target="_blank"><img src="http://techkidscamp.jp/common/img/hdr_btn_fb_on.png" alt="Facebook"></a></li>
         <li><a href="https://twitter.com/TechKidsCAMP" target="_blank"><img src="http://techkidscamp.jp/common/img/hdr_btn_tw_on.png" alt="Twitter"></a></li>
       </ul>
     </div>
   <!-- //END #globalNav -->
   </nav>
 <!-- //END #headerArea -->
 </header>
<!-- //END #wrapperTop -->
</div>