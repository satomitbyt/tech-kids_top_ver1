$(window).load(function(){

	var $nav_obj = $('#nav');
	var gnav_height;
	var scrollVal;

	$(window).resize(function() {
		gnavHeightFunc();
	});

	function gnavHeightFunc() {
		gnav_height = $('#globalNav').outerHeight() + $('#contact').outerHeight() + $('#socialNav').outerHeight() + 6;
	}

	gnavHeightFunc();

	$(window).scroll(function(){
		scrollVal = $(this).scrollTop();
		if (scrollVal > 0) {
			$('#wrapperTop').addClass('scroll');
		} else {
			$('#wrapperTop').removeClass('scroll');
		}
		if (scrollVal > 100) {
			$('#pageTop').fadeIn(300);
		} else {
			$('#pageTop').fadeOut(300);
		}
	});

	$(document).on('click', '#menu, #nav a', function(){
		var currentHeight = parseInt($nav_obj.css('height'));
		if (currentHeight === 0) {
			$nav_obj.stop(true, true).animate({
				'height'	: gnav_height
			}, {
				'easing'	: 'easeInQuad',
				'duration'	: 300
			});
		} else if (currentHeight === gnav_height) {
			$nav_obj.stop(true, true).animate({
				'height'	: 0
			}, {
				'easing'	: 'easeOutQuad',
				'duration'	: 300
			});
		}
	});

	$('#globalNav li a.over, .appBtn, .icoMap').on({
		'mouseenter': function() {
			$(this).stop(true, false).animate({'opacity': 0.6}, 300);
		},
		'mouseleave': function() {
			$(this).stop(true, false).animate({'opacity': 1}, 300);
		}
	});

	var devicewidth = window.innerWidth;
	if (document.getElementById("main_rwd") !== null) {
		imgChange();
	}

	$(window).resize(function(){
		devicewidth = window.innerWidth;
		if (document.getElementById("main_rwd") !== null) {
			imgChange();
		}
	});

	function imgChange() {
		if(devicewidth <= 612) {
			$('#main_rwd').attr('src', $('#main_rwd').attr('src').replace('.jpg', '_sp.jpg'));
		} else {
			$('#main_rwd').attr('src', $('#main_rwd').attr('src').replace('_sp.jpg', '.jpg'));
		}
	}

	$('#pageTop').css('display', 'none');
	$('#pageTop a').on('click', function(){
		$('html, body').animate({scrollTop: 0}, {easing: 'easeInCubic', duration: 300});
		return false;
	});

	$('.comingsoon').on('click', function(){
		return false;
	});
});



//2015/5/10追記
// スマートフォンで表示する場合に、電話番号をクリックで直接かけられるようにする
$(function(){
	var device = navigator.userAgent;
	if((device.indexOf('iPhone') > 0 && device.indexOf('iPad') == -1) || device.indexOf('iPod') > 0 || device.indexOf('Android') > 0){
		$("#contact").wrap('<a href="tel:0354590212"></a>');
	}
});









