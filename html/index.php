<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7 oldie" lang="ja"><![endif]-->
<!--[if (IE 7)&!(IEMobile)]><html class="no-js lt-ie9 lt-ie8 oldie" lang="ja"><![endif]-->
<!--[if (IE 8)&!(IEMobile)]><html class="no-js lt-ie9 oldie" lang="ja"><![endif]-->
<!--[if IE 9]><html class="no-js ie9" lang="ja"><![endif]-->
<!--[if gt IE 9]><!--><html class="no-js" lang="ja"><!--<![endif]-->
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">

<meta property="og:title" content="小学生・子ども向けのプログラミング教育 | プログラミングの Tech Kids School（テックキッズスクール）">
<meta property="og:site_name" content="小学生・子ども向けのプログラミング教育 | プログラミングの Tech Kids School（テックキッズスクール）">
<meta property="og:description" content="Tech Kids CAMP / Tech Kids Schoolは、HPやアプリの開発など、デジタルのモノづくりを楽しく学ぶことのできる、小学生のためのプログラミングスクールです。">
<meta property="og:type" content="website">
<meta property="og:url" content="http://techkidscamp.jp/">
<meta property="og:image" content="http://techkidscamp.jp/img/social.jpg">

<meta name="description" content="Tech Kids CAMP / Tech Kids Schoolは、HPやアプリの開発など、デジタルのモノづくりを楽しく学ぶことのできる、小学生のためのプログラミングスクールです。">
<meta name="keywords" content="Tech Kids CAMP,小学生プログラミング,サマーキャンプ,ワークショップ,テックキッズ">

<title>小学生・子ども向けのプログラミング教育 | プログラミングの Tech Kids School（テックキッズスクール）</title>

<link rel="dns-prefetch" href="//s3.amazonaws.com">
<link rel="dns-prefetch" href="//cdn.api.twitter.com">
<link rel="dns-prefetch" href="//connect.facebook.net">
<link rel="dns-prefetch" href="//themes.googleusercontent.com">
<link rel="dns-prefetch" href="//apis.google.com">
<link rel="dns-prefetch" href="//google-analytics.com">
<link rel="dns-prefetch" href="//ssl.google-analytics.com">

<link rel="stylesheet" href="common/css/reset.css" media="all">
<link rel="stylesheet" href="common/css/default.css" media="all">
<link rel="stylesheet" href="css/style.css" media="all">

<link rel="shortcut icon" href="http://techkidscamp.jp/common/img/favicon.ico">

<script src="http://code.jquery.com/jquery-1.10.0.min.js"></script>
<script src="http://cdnjs.cloudflare.com/ajax/libs/modernizr/2.5.3/modernizr.min.js"></script>
<script>
window.jQuery || document.write(unescape("%3Cscript src='common/js/jquery-1.10.0.min.js'%3E%3C/script%3E"));
window.jQuery || document.write(unescape("%3Cscript src='common/js/modernizr.min.js'%3E%3C/script%3E"));
</script>
<!--[if (gte IE 6)&(lte IE 8)]>
<script src="common/js/DOMAssistantCompressed-2.8.1.js"></script>
<script src="common/js/selectivizr-min.js"></script>
<script src="common/js/respond.min.js"></script>
<![endif]-->
<script src="http://techkidscamp.jp/common/js/jquery.easing.1.3.js"></script>
<script src="http://techkidscamp.jp/common/js/common.js"></script>
<script src="http://techkidscamp.jp/common/js/ga.js"></script>


<!-- flexslider -->
<script src="bxslider/jquery.bxslider.js"></script>
<link rel='stylesheet' href='bxslider/jquery.bxslider.css' type='text/css' media='all'/>
<script>

  $(window).load(function() {
    $('.bxslider').bxSlider({
      controls: true,
      auto: true
    });
  });

</script>


<script>

var userAgent = window.navigator.userAgent.toLowerCase();
var appVersion = window.navigator.appVersion.toLowerCase();

if (userAgent.indexOf('msie') != -1) {
  uaName = 'ie';
  if (appVersion.indexOf('msie 6.') != -1) {
    alert("当サイトはご利用のブラウザでは正しく表示されません。別のブラウザで閲覧していただくか、Internet Explorerを最新のバージョンにアップデートしてください。");
  } else if (appVersion.indexOf('msie 7.') != -1) {
    alert("当サイトはご利用のブラウザでは正しく表示されません。別のブラウザで閲覧していただくか、Internet Explorerを最新のバージョンにアップデートしてください。");
  } else if (appVersion.indexOf('msie 8.') != -1) {
    alert("当サイトはご利用のブラウザでは正しく表示されません。別のブラウザで閲覧していただくか、Internet Explorerを最新のバージョンにアップデートしてください。");
  } else if (appVersion.indexOf('msie 9.') != -1) {
    alert("当サイトはご利用のブラウザでは正しく表示されません。別のブラウザで閲覧していただくか、Internet Explorerを最新のバージョンにアップデートしてください。");
  }

</script>


<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-41755105-1', 'auto');
  ga('send', 'pageview');

</script>




</head>

<body>

<?php require $_SERVER["DOCUMENT_ROOT"]."/common/header.php" ?>

<div id="wrapperMiddle">

<div class="areaMainVisual">
  <ul class="bxslider">
   <li><a href="http://techkidscamp.jp/school/"><img src="img/keyVisual/visual01.jpg" class="pc" /><img src="img/keyVisual/sp_visual01.jpg" class="sp" /></a></li>
    <li><a href="http://techkidscamp.jp/camp/index.html"><img src="img/keyVisual/visual02.jpg" class="pc" /><img src="img/keyVisual/sp_visual02.jpg" class="sp" /></a></li>
    <!--<li><a href="http://techkidscamp.jp/camp/"><img src="img/keyVisual/visual00.jpg" class="pc" /><img src="img/keyVisual/sp_visual00.jpg" class="sp" /></a></li>
 -->
  </ul>
</div>





<div id="contentsArea" class="outer">
<div class="innerWrap">

<!--ここからmovie追加 2014.2.28-->
<section id="movieBox">
<div class="wrapper">
<div id="subBox">
  <div style="font-size:24px; font-weight:bold;">Welcome to Tech Kids!</div>
	<p style="font-size:1em;">
    Tech Kids CAMP / Tech Kids Schoolは、HPやアプリの開発など、デジタルのモノづくりを楽しく学ぶことのできる、小学生のためのプログラミングスクールです。<br>
    <br>
    インターネットメディア「Ameba」を運営するIT企業サイバーエージェントが運営しています。<br>
    <br>
    パソコンやスマートフォンをほとんど触ったことが無いというお子さんでも大丈夫！楽しい雰囲気の中で少人数制で学ぶことができます。<br>
    <br>
    <img src="img/powered_by_CA.png" />

  </p>
</div>
<div id="mainBox" style="padding-top:45px;"><iframe id="movie"  src="http://www.youtube.com/embed/eW2xrOmQjH0" frameborder="0" frameborder="0" allowfullscreen></iframe></div>
<div style="clear:both;"></div>
</div>
</section>
<!--追加ここまで-->


<section id="contentsBox" class="inner">
<h3><img src="http://techkidscamp.jp/img/cnt_ttl_information.png" alt="Information"></h3>

<div id="contentsList">



<!--
<dl>
<dt class="cntImg"><a href="https://www.cyberagent.co.jp/scholarship/"><img src="http://techkidscamp.jp/img/cnt_img_contents40.jpg" alt=""></a></dt>
<dt class="cntTtl"><a href="https://www.cyberagent.co.jp/scholarship/">キッズプログラマー奨学生</a></dt>
<dd>プログラミング学習の費用をサイバーエージェントが無償提供いたします。奨学生対象者を選考受付中！</dd>
</dl> -->

<dl>
<dt class="cntImg"><a href="http://techkidscamp.jp/camp/"><img src="img/cnt_img_contents92.png" alt=""></a></dt>
<dt class="cntTtl"><a href="http://techkidscamp.jp/camp/">Tech Kids CAMP Spring 2016</a></dt>
<dd><span style="color:red;">[ 2/15までのお申し込みで2,000円OFF！ ]</span><br>
春休み期間に開催する小学生のためのプログラミング入門ワークショップです。8つのコースから選択し、プログラミングによるモノづくりを体験できます。
</dd>
</dl>
</dl>

<dl>
<dt class="cntImg"><a href="http://techkidscamp.jp/school/"><img src="img/cnt_img_contents52.jpg" alt=""></a></dt>
<dt class="cntTtl"><a href="http://techkidscamp.jp/school/">Tech Kids School</a></dt>
<dd><span style="color:red;">[ お友達・ご兄弟の紹介で¥5,000キャッシュバック！ ]</span><br>日本最大の小学生向けプログラミングスクールです。毎週 / 隔週に１回の授業で、本格的なプログラミングを学びます。東京渋谷、名古屋名駅、大阪梅田、神戸三宮、福岡天神、沖縄那覇で開講！<br></dd>
</dl>


<dl>
<dt class="cntImg"><a href="http://techkidscamp.jp/hometeacher/" target="_blank"><img src="img/cnt_img_contents93.png" alt=""></a></dt>
<dt class="cntTtl"><a href="http://techkidscamp.jp/hometeacher/" target="_blank">Tech Kids Home Teacher</a></dt>
<dd><span style="color:red;">[ 3月31日までのお申込みで入会金0円！！ ]</span><br>小学生向けプログラミングスクール「Tech Kids School」認定講師の自宅で開講するプログラミング教室です。東京・神奈川・千葉・埼玉で4月より一斉開講！体験授業受付中！
</dd>
</dl>





</div>
<!-- <div id="contentsList"> -->



<div id="contentsList">


<dl>
<dt class="cntImg"><a href="http://techkidscamp.jp/minecraft/" target="_blank"><img src="img/cnt_img_contents90.png" alt=""></a></dt>
<dt class="cntTtl"><a href="http://techkidscamp.jp/minecraft/" target="_blank">Minecraftプログラミング体験会</a></dt>
<dd><span style="color:red;"><!--[ 東京・大阪・名古屋・福岡・沖縄で開催中！ ]--></span><br>Minecraft（マインクラフト）プログラミング体験会を無料で開催！
</dd>
</dl>

<dl>
<dt class="cntImg"><a href="https://reg31.smp.ne.jp/regist/is?SMPFORM=lgke-obqir-fbf39d30ea3e194bf47fe470a73a672e" target="_blank"><img src="http://techkidscamp.jp/img/cnt_img_contents87.jpg" alt=""></a></dt>
<dt class="cntTtl"><a href="https://reg31.smp.ne.jp/regist/is?SMPFORM=lgke-obqir-fbf39d30ea3e194bf47fe470a73a672e" target="_blank">メールマガジン登録</a></dt>
<dd>Tech Kids CAMP や Tech Kids School、各種イベントや、プログラミング学習の最新情報についてご案内するメールマガジンです。</dd>
</dl>

<dl>
    <dt class="cntImg"><a href="http://techkidscamp.jp/blog" target="new"><img src="http://techkidscamp.jp/img/cnt_img_contents41.jpg" alt=""></a></dt>
    <dt class="cntTtl"><a href="http://techkidscamp.jp/blog" target="new">Blog</a></dt>
    <dd>Tech Kids School 校長・上野のブログです。スクールやイベントの様子をご紹介いたします。</dd>
</dl>




</div>


<div id="contentsList">

<dl>
<dt class="cntImg"><a href="hometeacher/recruit/index.html" target="_blank"><img src="img/cnt_img_contents91.jpg" alt=""></a></dt>
<dt class="cntTtl"><a href="hometeacher/recruit/index.html" target="_blank">Tech Kids Home Teacher</a></dt>
<dd>CA Tech Kids フランチャイズ教室開講！自宅開講型小学生向けプログラミングサービス「Tech Kids Home Teacher」女性講師募集プレエントリー受付中！</dd>
</dl>



<dl>
<dt class="cntImg"><a href="event/"><img src="http://techkidscamp.jp/img/cnt_img_contents54.jpg" alt=""></a></dt>
<dt class="cntTtl"><a href="event/">Event Info</a></dt>
<dd>全国各地で開催するイベントの情報です。</dd>
</dl>

<dl>
<dt class="cntImg"><a href="http://techkidscamp.jp/event/2014/10/entry_28.html"><img src="http://techkidscamp.jp/img/cnt_img_contents53.jpg" alt=""></a></dt>
<dt class="cntTtl"><a href="http://techkidscamp.jp/event/2014/10/entry_28.html">生徒作品紹介</a></dt>
<dd>Tech Kids School の生徒が開発した作品をご紹介します。</dd>
</dl>



</div>



<div id="contentsList">

<dl>
<dt class="cntImg"><a href="release/"><img src="http://techkidscamp.jp/img/cnt_img_contents39.jpg" alt=""></a></dt>
<dt class="cntTtl"><a href="release/">Press Release</a></dt>
<dd>プレスリリースをご紹介いたします。</dd>
</dl>

<dl>
<dt class="cntImg"><a href="public/"><img src="http://techkidscamp.jp/img/cnt_img_contents51.jpg" alt=""></a></dt>
<dt class="cntTtl"><a href="public/">小学校・自治体との取り組み</a></dt>
<dd>プログラミング教育の推進に向けた小学校・地方自治体との取り組みをご紹介いたします。</dd>
</dl>

<dl>
    <dt class="cntImg"><a href="http://techkidscamp.jp/gamecreator_2015/"><img src="img/cnt_img_contents89.jpg" alt=""></a></dt>
    <dt class="cntTtl"><a href="http://techkidscamp.jp/gamecreator_2015/">ゲームクリエイター講座<br>with SUPER MARIO MAKER</a></dt>
    <dd>任天堂と共催でゲームクリエイターを目指す子どもたちに特別講座を開催！
    </dd>
</dl>






<!-- //END #contentsList -->


</div>

<div id="contentsList">

  <dl>
    <dt class="cntImg"><a href="http://techkidscamp.jp/scratch/index.html"><img src="img/cnt_img_contents84.jpg" alt=""></a></dt>
    <dt class="cntTtl"><a href="http://techkidscamp.jp/scratch/index.html">Scratchゲーム開発講座</a></dt>
    <dd><span style="color:red;"></span>Scratchでプログラミングに挑戦しよう！</dd>
</dl>


<dl>
<dt class="cntImg"><a href="techkidsmentors/"><img src="http://techkidscamp.jp/img/cnt_img_contents56.jpg" alt=""></a></dt>
<dt class="cntTtl"><a href="techkidsmentors/">Tech Kids Mentors</a></dt>
<dd>お子様のプログラミング学習をサポートするメンター（インストラクター）をご紹介いたします。</dd>
</dl>

<dl>
<dt class="cntImg"><a href="http://techkidscamp.jp/company/"><img src="http://techkidscamp.jp/img/cnt_img_contents05.jpg" alt=""></a></dt>
<dt class="cntTtl"><a href="http://techkidscamp.jp/company/">運営会社</a></dt>
<dd>Tech Kids CAMP / Tech Kids School を運営する株式会社CA Tech Kids についてご紹介いたします。</dd>
</dl>





<!-- //END #contentsList --></div>




<div id="contentsList">

  <dl>
    <dt class="cntImg"><a href="http://life-is-tech.com/"><img src="http://techkidscamp.jp/img/cnt_img_contents30.jpg" alt=""></a></dt>
    <dt class="cntTtl"><a href="http://life-is-tech.com/">中学生・高校生はLife is Tech!</a></dt>
    <dd>中学生・高校生の方は、中高生向けプログラミングキャンプ Life is Tech! に是非ご参加ください。</dd>
</dl>



</div>


<div id="socialBox">
<div id="fbBlock">
<iframe src="//www.facebook.com/plugins/likebox.php?href=https%3A%2F%2Fwww.facebook.com%2Ftechkids.jp&amp;width=292&amp;height=590&amp;colorscheme=light&amp;show_faces=true&amp;header=true&amp;stream=true&amp;show_border=false&amp;appId=721573681202830" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:100%;" allowTransparency="true"></iframe>
</div>
<div id="twBlock">
<a class="twitter-timeline" width="520" height="489" href="https://twitter.com/TechKidsCAMP" data-widget-id="375436812837060608">@TechKidsCAMP からのツイート</a>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
</div>
</div>
<!-- //END #contentsBox --></section>
<!-- //END .innerWrap --></div>
<!-- //END #contentsArea --></div>
<!-- //END #wrapperMiddle --></div>

<div id="wrapperBottom">
<footer id="footerArea" class="inner">
<nav id="footerNav">
<ul>
<li><a href="./">ホーム</a></li>
<li><a href="camp/">キャンプ</a></li>
<li><a href="school/">スクール</a></li>
<li><a href="event/">イベント情報</a></li>
<li><a href="https://reg31.smp.ne.jp/regist/is?SMPFORM=lgke-obqir-fbf39d30ea3e194bf47fe470a73a672e">メルマガ登録</a></li>
<li><a href="company/">運営会社</a></li>
<li><a href="https://reg31.smp.ne.jp/regist/is?SMPFORM=lgke-obsjt-3c793296d9a768ffa8c895846bd64e39">お問い合わせ</a></li>
</ul>
<!-- //END #footerNav --></nav>
<p id="pageTop"><a href="#"><img src="http://techkidscamp.jp/common/img/cnt_ico_pagetop.png" alt="Page Top"></a></p>
<p id="copyright"><small>Copyright &copy; 2013 CA Tech Kids, Inc. All Rights Reserved.</small></p>
<!-- //END #footerArea --></footer>
<!-- //END #wrapperBottom --></div>

</body>
</html>
